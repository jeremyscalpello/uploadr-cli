#!/usr/bin/env node

var args = require('yargs').argv;
var path = require('path');
var fs = require('fs');
var colors = require('colors');
var mime = require('mime');
var prompt = require('prompt');
var misc = require('../lib/misc');
var upload = require('../lib/upload');

var argSchema = {
  lifetime: [1, 4, 8, 24, 48, 72],
  downloads: [1, 2, 5, 10],
  password: [true, false],
  verbose: [true, false]
};
var options = {};
var fileObj = {};

var cwd = process.cwd();
var file = args._[0];
global.v = false;

global.transfer = {};

misc.showLogo();

if (args.help || args.h) return misc.showHelp();
else if (!file) return misc.exit('No File Specified', true);
else {
  if (!path.isAbsolute(file)) {
    if (file.substr(0, 2) != './') file = '/' + file;
    else file = file.substr(1, file.length)
    file = cwd + file;
  }
  file = path.normalize(file);
  console.log(('Uploading file ' + file).green);
}
fs.stat(file, function (err, stats) {
  if (err) return misc.exit('File Not Found', true);
  else if (stats.size > misc.config.maxSize) return misc.exit('File is too large. Limit is ' + misc.formatBytes(misc.config.maxSize));
  else {
    console.log(('File size: ' + misc.formatBytes(stats.size)).green);
    Object.keys(argSchema).forEach(function (key) {
      var val = args[key] || args[key.substr(0, 1)] || misc.config.defaults[key];
      if (key == 'verbose') v = val;
      options[key] = val;
      if (argSchema[key].indexOf(val) == -1) return misc.exit('Invalid value for ' + key + ' provided', true);
    });
    fileObj = {
      path: file,
      fileName: file.split('/')[file.split('/').length - 1],
      fileSize: stats.size,
      mimeType: mime.lookup(file),
      lifetime: options.lifetime || misc.config.defaults.lifetime,
      maxDownloads: options.downloads || misc.config.defaults.downloads
    };
    if (options.password) {
      prompt.start();
      prompt.get({
        properties: {
          password: {
            hidden: true,
            replace: '*'
          }
        }
      }, function (err, result) {
        if (!err && result.password) fileObj.password = result.password;
        return upload(fileObj);
      });
    } else return upload(fileObj);

  }
});
