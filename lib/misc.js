var fs = require('fs');

exports.config = {
  apiUrl: 'https://uploadr.eu/api/v1/',
  maxSize: 10000000000,
  defaults: {
    lifetime: 48,
    downloads: 10,
    verbose: false,
    password: false
  }
};

exports.showHelp = function () {
  fs.readFile(__dirname + '/help.txt', 'utf8', function (err, help) {
    if (err) {
      console.log('Failed to get help');
      process.exit(1);
    } else {
      console.log(help);
      process.exit(0);
    }
  });
};

exports.showLogo = function () {
  var logo = fs.readFileSync(__dirname + '/logo.txt', 'utf8');
  if (logo) console.log(logo);
};

exports.exit = function (message, err) {
  console.log(message.red.underline);
  if (err) console.log('Use the --help option to get help'.red);
  process.exit(err ? 1 : 0);
};

exports.formatBytes = function (bytes, decimals) {
  if (!decimals) decimals = 2;
  if (bytes == 0) return '0 Bytes';
  var k = 1024;
  var dm = decimals + 1 || 3;
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  var i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
