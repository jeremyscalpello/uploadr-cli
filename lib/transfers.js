var requestJson = require('request-json');
var request = require('request');
var fs = require('fs');
var streamToBuffer = require('stream-to-buffer')
var misc = require('../lib/misc');

var api = requestJson.createClient(misc.config.apiUrl);

exports.startTransfer = function (opts, callback) {
  api.post('transfers', opts, function (err, headers, data) {
    if (err) return callback(err);
    else if (data && data.code == 200) {
      transfer.token = data.token;
      transfer.id = data.transfer;
      transfer.url = data.url;
      transfer.chunkSize = data.chunkSize;
      transfer.chunksCompleted = 0;
      transfer.numChunks = 0;
      return callback(null, data);
    } else return callback(data);
  });
};

exports.signPart = function (partNumber, callback) {
  api.post('transfers/' + transfer.id + '/parts/' + partNumber, {}, function (err, headers, data) {
    if (err) return callback(err);
    else if (data && data.code == 200) {
      return callback(null, data.signedUrl);
    } else return callback(data);
  });
};

exports.uploadPart = function (signedUrl, part, callback, progressCallback) {
  var stream = fs.createReadStream(part.filePath, {
    start: part.start,
    end: part.end
  });
  streamToBuffer(stream, function (err, buffer) {
    if (err) return next(err);
    else {
      var req = request({
        method: 'PUT',
        uri: signedUrl,
        body: buffer
      });
      req.on('error', function (err) {
        if (err) return callback(err);
      });
      req.on('response', function (res) {
        if (res.statusCode != 200) return callback(res.headers);
        else {
          var etag = res.headers['etag'];
          etag = etag.split('"').join('');
          return callback(null, etag);
        }
      });
      setInterval(function () {
        if (req.req && req.req.connection && req.req.connection.bytesWritten) progressCallback(req.req.connection.bytesWritten);
      }, 500);
    }
  });
};

exports.completePart = function (partNumber, etag, callback) {
  api.put('transfers/' + transfer.id + '/parts/' + partNumber, {
    etag: etag
  }, function (err, headers, data) {
    if (err) return callback(err);
    else if (data && data.code == 200) {
      return callback(null, data);
    } else return callback(data);
  });
};

exports.completeUpload = function (callback) {
  api.post('transfers/' + transfer.id + '/complete', {}, function (err, headers, data) {
    if (err) return callback(err);
    else if (data && data.code == 200) {
      return callback(null, data);
    } else return callback(data);
  });
};

exports.checkCompletion = function (callback) {
  api.get('transfers/' + transfer.id + '/status', function (err, headers, data) {
    if (err) return callback(err);
    else if (data && data.code == 200) {
      return callback(null, data.message);
    } else return callback(data);
  });
};
