var helpers = require('./transfers');
var misc = require('./misc');
var async = require('async');
var ProgressBar = require('progress');

var bar;
var percent = 0;

module.exports = function (file) {
  helpers.startTransfer(file, function (err, data) {
    if (err) {
      if (v) console.log(err);
      return misc.exit('An error occurred', true);
    } else {
      transfer.numChunks = Math.ceil(file.fileSize / transfer.chunkSize);
      var start = 0;
      var end;
      for (var a = 0; a < transfer.numChunks; a++) {
        start = transfer.chunkSize * a;
        end = Math.min(start + transfer.chunkSize, file.fileSize);
        queue.push({
          number: a + 1,
          start: start,
          end: end,
          chunkSize: transfer.chunkSize,
          filePath: file.path,
          fileSize: file.fileSize
        });
      }
      bar = new ProgressBar('  uploading [:bar] :percent', {
        total: 100
      });
    }
  });
};

var queue = async.queue(function (part, callback) {
  async.retry(10, function (cb) {
    helpers.signPart(part.number, function (err, signedUrl) {
      if (err) return cb(err);
      else {
        helpers.uploadPart(signedUrl, part, function (err, etag) {
          if (err || !etag) return cb(err || 'NoEtag');
          else {
            helpers.completePart(part.number, etag, function (err) {
              if (err) return cb(err);
              else return cb();
            });
          }
        }, function (uploadedBytes) {
          var per = Math.ceil(((uploadedBytes + (transfer.chunksCompleted * transfer.chunkSize)) / part.fileSize) * 100);
          if (per != percent && percent <= 100) {
            bar.tick(per - percent);
            percent = per;
          }
        });
      }
    });
  }, function (err, result) {
    if (err) {
      if (v) console.log(err);
      misc.exit('File upload failed', true);
      queue.kill();
    } else {
      transfer.chunksCompleted++;
      return callback();
    }
  });
});

queue.drain = function () {
  bar.tick(100 - percent);
  console.log('Completing your upload'.green);
  helpers.completeUpload(function (err) {
    if (err) {
      if (v) console.log(err);
      return misc.exit('File upload failed', true);
    } else {
      console.log('Finishing off'.green);
      (function startCompletionCheck() {
        helpers.checkCompletion(function (err, message) {
          if (message == 'Complete') {
            console.log('*************************************************************************'.blue);
            console.log('  Upload Complete'.blue);
            console.log(('  Your file is available at ' + transfer.url).blue);
            console.log('*************************************************************************'.blue);
            process.exit(0);
          } else setTimeout(startCompletionCheck, 800);
        });
      })();
    }
  });
};
